# Используем официальный Python образ из Docker Hub
FROM python:3.10

# Устанавливаем рабочую директорию в контейнере
WORKDIR /app

# Копируем все файлы в рабочую директорию
COPY .. /app

# Устанавливаем зависимости из req.txt
RUN pip install --no-cache-dir -r req.txt

# Копируем файл .env в рабочую директорию
COPY .env .env

# Определяем переменные окружения
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0


# Открываем порт
EXPOSE 5000

# Запуск приложения
CMD ["flask", "run", "--host=0.0.0.0"]
