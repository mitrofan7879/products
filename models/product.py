import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from dotenv import find_dotenv, load_dotenv

# Загрузка переменных окружения из файла .env
load_dotenv(find_dotenv())

# Получение строки подключения к базе данных из переменной окружения
DB = os.getenv('DB')

# Создание экземпляра Flask приложения
app = Flask(__name__, template_folder='../templates')
app.config['SQLALCHEMY_DATABASE_URI'] = DB
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Создание экземпляра SQLAlchemy
db = SQLAlchemy(app)


class Product(db.Model):
    """Модель продукта для базы данных"""

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.Text, nullable=True)
    price = db.Column(db.Float, nullable=False)
    category = db.Column(db.String(50), nullable=True)

    def to_dict(self):
        """Возвращает словарь с данными продукта"""
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'price': self.price,
            'category': self.category
        }

    def __repr__(self):
        """Возвращает строковое представление продукта"""
        return f'<Product {self.name}>'

    def serialize(self):
        """Сериализует данные продукта в словарь"""
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'price': self.price,
            'category': self.category
        }
