import os
import sys
import unittest
import requests

from models.product import app, db

# Добавление пути к корневому каталогу проекта
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

url = 'http://localhost:5000'


class FlaskApiTests(unittest.TestCase):
    def setUp(self):
        """Настройка тестового окружения перед каждым тестом"""
        requests.testing = True
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        with app.app_context():
            db.create_all()

    def tearDown(self):
        """Очистка тестового окружения после каждого теста"""
        with app.app_context():
            db.session.remove()
            db.drop_all()
            db.create_all()

    def test_index_route(self):
        """Тестирование начального маршрута (главной страницы)"""
        response = requests.get(url)
        self.assertEqual(response.status_code, 200)

    def test_get_products_empty(self):
        """Тестирование получения списка продуктов, когда список пуст"""
        response = requests.get(url + '/products')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])

    def test_create_product(self):
        """Тестирование создания нового продукта"""
        response = requests.post(url + '/products', json={
            'name': 'Test Product',
            'description': 'Test Description',
            'price': 19.99,
            'category': 'Test Category'
        })
        self.assertEqual(response.status_code, 201)
        self.assertIn('Test Product', response.json()['name'])

    def test_get_product(self):
        """Тестирование получения конкретного продукта по его ID"""
        # Сначала создаем продукт
        response = requests.post(url + '/products', json={
            'name': 'Another Test Product',
            'description': 'Another Description',
            'price': 29.99,
            'category': 'Another Category'
        })
        product_id = response.json()['id']

        # Теперь получаем продукт по его ID
        response = requests.get(url + f'/products/{product_id}')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['name'], 'Another Test Product')

    def test_update_product(self):
        """Тестирование обновления данных продукта"""
        # Создаем продукт для обновления
        response = requests.post(url + '/products', json={
            'name': 'Old Name',
            'description': 'Old Description',
            'price': 9.99,
            'category': 'Old Category'
        })
        product_id = response.json()['id']

        # Обновляем данные продукта
        response = requests.put(url + f'/products/{product_id}', json={
            'name': 'New Name',
            'description': 'New Description',
            'price': 10.99,
            'category': 'New Category'
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('New Name', response.json()['name'])

    def test_delete_product(self):
        """Тестирование удаления продукта"""
        # Создаем продукт для удаления
        response = requests.post(url + '/products', json={
            'name': 'To be deleted',
            'description': 'Delete me',
            'price': 0.99,
            'category': 'Delete Category'
        })
        product_id = response.json()['id']

        # Удаляем продукт
        response = requests.delete(url + f'/products/{product_id}')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Product deleted', response.json()['message'])

    def test_search_products(self):
        """Тестирование поиска продуктов"""
        # Добавляем продукты для поиска
        requests.post(url + '/products', json={
            'name': 'Search Product 1',
            'description': 'First Test',
            'price': 5.00,
            'category': 'Search Test'
        })
        requests.post(url + '/products', json={
            'name': 'Search Product 2',
            'description': 'Second Test',
            'price': 15.00,
            'category': 'Search Test'
        })

        # Тестируем поиск продукта
        response = requests.get(url + '/search?query=Product')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.json()) > 0)  # Проверяем, что поиск возвращает результаты


if __name__ == '__main__':
    unittest.main()
    with app.app_context():
        db.create_all()
