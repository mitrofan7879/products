from flask import jsonify, request, render_template, redirect, url_for
from sqlalchemy import or_, asc, desc
from models.product import Product, app, db
import json


@app.route('/')
def index():
    """Рендеринг главной страницы"""
    return render_template('index.html')


@app.route('/products', methods=['GET'])
def get_products():
    """Получение списка всех продуктов"""
    products = Product.query.all()
    products_list = [product.__dict__ for product in products]
    for product in products_list:
        product.pop('_sa_instance_state', None)  # Удаление служебного атрибута SQLAlchemy
    return app.response_class(
        response=json.dumps(products_list, ensure_ascii=False),
        mimetype='application/json'
    )


@app.route('/products/<int:product_id>', methods=['GET'])
def get_product(product_id):
    """Получение информации о конкретном продукте по его ID"""
    product = Product.query.get_or_404(product_id)
    product_dict = product.__dict__
    product.to_dict()
    product_dict.pop('_sa_instance_state', None)  # Удаление служебного атрибута SQLAlchemy
    return app.response_class(
        response=json.dumps(product_dict, ensure_ascii=False),
        mimetype='application/json'
    )
    # return render_template('product.html', product=product)


@app.route('/products', methods=['POST'])
def create_product():
    """Создание нового продукта"""
    data = request.json
    new_product = Product(name=data['name'], description=data['description'],
                          price=data['price'], category=data['category'])

    db.session.add(new_product)
    db.session.commit()
    product_dict = new_product.__dict__
    new_product.to_dict()
    product_dict.pop('_sa_instance_state', None)  # Удаление служебного атрибута SQLAlchemy
    return app.response_class(
        response=json.dumps(product_dict, ensure_ascii=False),
        mimetype='application/json'
    ), 201
    # print(product_dict)
    # return jsonify(product_dict), 201


@app.route('/products/<int:product_id>', methods=['PUT'])
def update_product(product_id):
    """Обновление информации о продукте по его ID"""
    product = Product.query.get_or_404(product_id)
    data = request.json
    product.name = data['name']
    product.description = data['description']
    product.price = data['price']
    product.category = data['category']
    db.session.commit()
    product_dict = product.__dict__
    product.to_dict()
    product_dict.pop('_sa_instance_state', None)  # Удаление служебного атрибута SQLAlchemy
    return app.response_class(
        response=json.dumps(product_dict, ensure_ascii=False),
        mimetype='application/json'
    )


@app.route('/products/<int:product_id>', methods=['DELETE'])
def delete_product(product_id):
    """Удаление продукта по его ID"""
    product = Product.query.get_or_404(product_id)
    db.session.delete(product)
    db.session.commit()
    return jsonify({'message': 'Product deleted'}), 200


@app.route('/search', methods=['GET'])
def search_products():
    """Поиск продуктов по заданному запросу с возможностью сортировки"""
    query = request.args.get('query')
    sort_by = request.args.get('sort_by', 'name')
    sort_order = request.args.get('sort_order', 'asc')

    if not query:
        return jsonify(error='Query parameter "query" is required'), 400

    products = Product.query.filter(
        or_(Product.name.ilike(f"%{query}%"), Product.description.ilike(f"%{query}%"))
    )

    if sort_order == 'asc':
        products = products.order_by(asc(getattr(Product, sort_by)))
    else:
        products = products.order_by(desc(getattr(Product, sort_by)))

    products_list = [
        {
            'id': product.id,
            'name': product.name,
            'description': product.description,
            'price': product.price,
            'category': product.category,
            'url': url_for('get_product', product_id=product.id)
        }
        for product in products.all()
    ]
    #return render_template('search_results.html', products=products_list, query=query, sort_by=sort_by, sort_order=sort_order)
    return app.response_class(
        response=json.dumps(products_list, ensure_ascii=False),
        mimetype='application/json'
    )


@app.route('/add_product', methods=['GET', 'POST'])
def add_product():
    """Добавление нового продукта через форму"""
    if request.method == 'POST':
        name = request.form['name']
        description = request.form['description']
        price = float(request.form['price'])
        category = request.form['category']

        new_product = Product(name=name, description=description, price=price, category=category)
        db.session.add(new_product)
        db.session.commit()

        return redirect(url_for('get_product', product_id=new_product.id))

    return render_template('add_product.html')


if __name__ == '__main__':
    with app.app_context():
        db.create_all()
    app.run(host='0.0.0.0', port=5000, debug=True)
