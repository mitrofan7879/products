≡ Краткое руководство

## Setup

1. **Clone the repository:**

   ```bash
   git clone https://gitlab.com/mitrofan7879/products
   cd products
2. **Set up virtual environment (optional):**

   ```bash
   docker-compose up --build

## Usage
Running the Application without docker-compose
1. Set up virtual environment (optional):
    ```python -m venv venv
   source venv/bin/activate  # On Windows use `venv\Scripts\activate`
   ```
2. Install dependencies:
    ```
   pip install -r requirements.txt
   ```
3. Run application:
   ```bash
   python app.py
   ```
The application will start running at http://localhost:5000.

## Run unittest
Сonfigure the virtual environment from the previous point and use script:
```bash
   python -m unittest tests.test_api
   ```

CRUD operations are located here
```bash
   python CRUD/get.py
   python CRUD/post.py
   python CRUD/put.py
   python CRUD/delete.py
   ```
## Example Requests

You can use tools like curl or Postman to interact with the API. Here are some sample requests

1. GET Request (Retrieve all products):
    ```bash
   curl http://localhost:5000/products
   ```
2. POST Request (Create a new product):
    ```bash
   curl -X POST -H "Content-Type: application/json" -d '{
    "name": "New Product",
    "description": "Description of the new product",
    "price": 24.99,
    "category": "New Category"
   }' http://localhost:5000/products
   ```
3. PUT Request (Update an existing product):
    ```bash
   curl -X PUT -H "Content-Type: application/json" -d '{
    "name": "Updated Product Name",
    "description": "Updated description",
    "price": 29.99,
    "category": "Updated Category"
    }' http://localhost:5000/products/1
   ```

4. DELETE Request (Delete a product):
    ```bash
   curl -X DELETE http://localhost:5000/products/1
   ```
## Searching by sort_by and sort_order
   ```bash
   http://127.0.0.1:5000/search?query={poduct}&sort_by={property}&sort_order={asc or desc}