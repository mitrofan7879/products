import requests

product_id = 1  # Замените на существующий ID продукта, который нужно обновить
url = f'http://localhost:5000/products/{product_id}'

updated_product = {
    'name': 'Updated Product Name',
    'description': 'Updated description of the product',
    'price': 29.99,
    'category': 'Updated Category'
}

response = requests.put(url, json=updated_product)

if response.status_code == 200:
    updated_product = response.json()
    print(f"Product updated successfully: {updated_product}")
else:
    print(f"Failed to update product: {response.status_code}")
