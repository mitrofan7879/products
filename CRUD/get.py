import requests

product_id = 1
url = f'http://localhost:5000/products/{product_id}'

response = requests.get(url)

if response.status_code == 200:
    product = response.json()
    print(f"Product: {product}")
else:
    print(f"Failed to retrieve products: {response.status_code}")
