import requests

url = 'http://localhost:5000/products'

new_product = {
    'name': 'Milk',
    'description': 'Description of the new product',
    'price': 24.99,
    'category': 'New Category'
}

response = requests.post(url, json=new_product)

if response.status_code == 201:
    created_product = response.json()
    print(f"New product {created_product['name']} created with ID: {created_product['id']}")
else:
    print(f"Failed to create product: {response.status_code}")
