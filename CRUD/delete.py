import requests

product_id = 1  # Замените на существующий ID продукта, который нужно удалить
url = f'http://localhost:5000/products/{product_id}'

response = requests.delete(url)

if response.status_code == 200:
    print(f"Product {product_id} {response.json()['name']} deleted successfully")
else:
    print(f"Failed to delete product: {response.status_code}")
